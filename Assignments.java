package com.javatraining;

import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Assignments {

	public int[] getFibonacci(int range) {
		int[] arr = new int[range];
		arr[0] = 0;
		arr[1] = 1;
		for (int i = 2; i < arr.length; i++) {
			arr[i] = arr[i - 1] + arr[i - 2];
		}
		return arr;
	}

	public long getFact(int num) {
		long fact = 1;
		if (num >= 1) {
			fact = num * getFact(num - 1);
			return fact;
		} else return 1;
	}
		

	public void swapNum(int x, int y) {
		System.out.println("x=" + x + " y=" + y); //x=8, y = 3
		x = y - x;// x = 3 - 8 = -5
		y = y - x;//y = 3 - (-5) = 8
		x = x + y;// x = -5 + 8 = 3
		System.out.println("After swapping\nx=" + x + " y=" + y);
	}

	public int getLargest(int x, int y, int z) {
		System.out.println("largest among " + x + "," + y + "," + z + " is:");
		int largest = (x > y && x > z) ? x : (y > x && y > z) ? y : z;
		return largest;
	}

	public String checkLeapyear(int year) {
		System.out.print("The year " + year + " is ");
		String status = null;
		if (year % 100 == 0) {
			status = (year % 400 == 0) ? "leapyear" : "not leapyear";
		} else {
			status = (year % 4 == 0) ? "leapyear" : "not leapyear";
		}
		return status;
	}

	public int reverseNum(int num) {
		System.out.print("reverse of " + num + "=");
		int rev = 0;
		while (num > 0) {
			int r = num % 10;
			num = num / 10;
			rev = rev * 10 + r;
		}
		return rev;
	}

	public String checkEvenOdd(int num) {
		System.out.print(num + " is ");
		return (num % 2) == 0 ? "even" : "odd";
	}

	public String checkPrime(int num) {
		System.out.print(num + " is ");
		String status = null;
		if (num == 0 || num == 1) {
			status = "neither prime nor not prime";
		} else if (num == 2) {
			status = "prime";
		} else {
			for (int i = 2; i < num; i++) {
				if (num % i == 0) {
					status = "not prime";
					break;
				} else
					status = "prime";
			}
		}
		return status;
	}

	public String checkPalindrome(String s) {
		s = s.toLowerCase();
		String rev = "";
		int length = s.length();
		for (int i = length - 1; i >= 0; i--)
			rev = rev + s.charAt(i);
		if (s.equals(rev))
			return (s + " is a palindrome");
		else
			return (s + " is not a palindrome");
	}

	public int sumArray(int[] arr) {
		int sum = 0;
		for (int a = 0; a < arr.length; a++) {
			sum = sum + arr[a];
		}
		return sum;
	}

	public String reverseString(String s) {
		String rev = "";
		for (int i = s.length() - 1; i >= 0; i--) {
			rev = rev + s.charAt(i);
		}
		return ("Reverse of " + s + " is: " + rev);
	}

	public void sortArray(int[] arr) {
		System.out.println("\nthe sorted array is:");
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr.length; j++) {
				if (arr[i] > arr[j]) {
					int x = arr[i];
					arr[i] = arr[j];
					arr[j] = x;
				}
			}
		}

		for (int v : arr) {
			System.out.print(v + " ");
		}
	}

	public int returnNthLargest(int[] arr, int pos) {
		Set<Integer> set = new LinkedHashSet<Integer>();
		System.out.print("The " + pos + "th" + " largest number is : ");
			for (int i = 0; i < arr.length; i++) {
				for (int j = 0; j < arr.length; j++) {
					if (arr[i] > arr[j]) {
						int x = arr[i];
						arr[i] = arr[j];
						arr[j] = x;	
					}
				}
			}
			for (int i : arr) {
				set.add(i);
			}
			if (pos <= set.size()) {
				return (Integer) set.toArray()[pos - 1];
			}
			else return -1;
	}

	public Map<String, Integer> returnDistinctNo(String[] arr) {
		Map<String, Integer> count = new LinkedHashMap<String, Integer>();
		for (int i = 0; i < arr.length; i++) {
			int occ = 0;
			for (int j = 0; j < arr.length; j++) {
				if (arr[i].equals(arr[j])) {
					occ++;
					count.put(arr[i], occ);
				}
			}
		}
		return count;

	}

	public long genrateRandomnum(int length) {
		double size = Math.pow(10, length);
		System.out.print("Random number of length " + length + " is:");
		return (long) (Math.random() * size);
	}

	public String findString(String str, int pos) {
		List<String> numbers = new LinkedList<String>();
		String num = "";
		Pattern pattern = Pattern.compile("[0-9]+");
		Matcher matcher = pattern.matcher(str);
		while (matcher.find()) {
			 numbers.add(matcher.group());
		} 
		num = numbers.get(pos - 1);
		return num;
	}

	public static void main(String[] args) {
		Assignments a = new Assignments();
		System.out.println("Fibonacci series:");
		int[] fibos = a.getFibonacci(8);
		for (int fibo : fibos) {
			System.out.print(fibo + " ");
		}
		System.out.println("\n");
		System.out.println(a.getFact(5));
		System.out.print("\n");
		a.swapNum(81, 33);
		System.out.print("\n");
		System.out.println(a.getLargest(-73, 31, 57));
		System.out.print("\n");
		System.out.println(a.checkLeapyear(2020));
		System.out.print("\n");
		System.out.println(a.reverseNum(1973));
		System.out.print("\n");
		System.out.println(a.checkEvenOdd(23));
		System.out.print("\n");
		System.out.println(a.checkPrime(26));
		System.out.print("\n");
		System.out.println(a.checkPalindrome("tacocat"));
		System.out.print("\nSum of array is = ");
		System.out.println(a.sumArray(fibos));
		System.out.print("\n");
		System.out.println(a.reverseString("batman"));
		System.out.print("\n");
		int[] arr = { 4, 3, 1, 9, 4, 5, 2, 1 };
		System.out.println("the input array is");
		for (int n : arr) {
			System.out.print(n + " ");
		}
		a.sortArray(arr);
		System.out.println("\n");
		int large = (a.returnNthLargest(arr, 6));
		if (large != -1) {
			System.out.println(large);
		} else {
			System.out.println("there are not so many distinct elements");
		}
		System.out.print("\n");
		String[] strs = { "elephant", "cat", "dog", "cat", "monkey", "bird", "bird" };
		Map<String, Integer> count = a.returnDistinctNo(strs);
		for (Map.Entry<String, Integer> m : count.entrySet()) {
			System.out.println(m.getKey() + " " + m.getValue());
		}
		System.out.print("\n");
		System.out.println(a.genrateRandomnum(2));
		System.out.print("\n");
		System.out.println(a.findString("your order numbered 12345 is placed and will be delivered on 15 th august, 2022", 2));
	}
}
