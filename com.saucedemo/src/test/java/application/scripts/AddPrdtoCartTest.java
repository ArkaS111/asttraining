package application.scripts;

import application.pages.CartPage;
import application.pages.LoginPage;
import application.pages.ProductsPage;
import library.Browser;
import library.Excel;
import library.Reporter;

public class AddPrdtoCartTest {

	public static void main(String[] args) {
		Reporter r = new Reporter(".\\src\\test\\resources\\AddPrdtoCartTestReport.html");
		Excel xl = new Excel(".\\src\\test\\resources\\TestData.xlsx");
		Browser browser = new Browser();
		xl.focusSheet(0);
		r.startTest("Add Product to Cart Test");
		browser.init();
		browser.maximize();
		String url = xl.getCelldata(1, "URL");
		browser.navigateTo(url);
		String username = xl.getCelldata(1, "UserName");
		String password = xl.getCelldata(1, "Password");
		LoginPage loginPage = new LoginPage(browser, r);
		loginPage.login(username, password);
		ProductsPage productsPage = new ProductsPage(browser, r);
		xl.focusSheet(1);
		int rownum = xl.getrowsNum();
		for (int i = 1; i <= rownum; i++) {
			String prdTitle = xl.getCelldata(i, "ProductName");
			productsPage.clickAddtoCart(prdTitle);
		}
		productsPage.clickCarticon();
		if (browser.checkNumberofPrd(rownum, CartPage.allItemxp())) {
			System.out.println("Scenario successfully executed");
			r.pass("Verification passed");
		} else {
			System.out.println("Scenario failed");
			r.fail("Scenario failed to execute succesfully");
		}
		r.endTest();
		r.flush();
		browser.close();
	}

}
