package application.scripts;

import application.pages.CartPage;
import application.pages.CompletePage;
import application.pages.LoginPage;
import application.pages.OverviewPage;
import application.pages.ProductsPage;
import application.pages.YourInformationPage;
import library.Base;
import library.Browser;
import library.Excel;
import library.Reporter;

public class EndToEnd {
	public static void main(String[] args) {
		Reporter r = new Reporter(".\\src\\test\\resources\\Output.html");
		Excel xl = new Excel(".\\src\\test\\resources\\TestData.xlsx");
		Browser browser = new Browser();
		LoginPage loginPage = new LoginPage(browser , r);
		ProductsPage productsPage = new ProductsPage(browser, r);
		xl.focusSheet(0);
		int rowsNum = xl.getrowsNum();
		r.startTest("demoTest");
		for (int i = 1; i <= rowsNum; i++) {
			browser.init();
			browser.maximize();
			String url = xl.getCelldata(1, "URL");
			browser.navigateTo(url);
			String username = xl.getCelldata(i, "UserName");
			String password = xl.getCelldata(i, "Password");
			loginPage.login(username, password);
			xl.focusSheet(1);
			String prod = xl.getCelldata(1,"ProductName");
			productsPage.clickAddtoCart(prod);
			String prod2 = xl.getCelldata(3 , "ProductName");
			productsPage.clickAddtoCart(prod2);
			browser.selectOptionByDisplay(ProductsPage.sortXP(), "Price (low to high)");
			productsPage.clickCarticon();
			CartPage cartpage = new CartPage(browser , r);
			cartpage.clickCheckout();
			YourInformationPage yip = new YourInformationPage(browser, r);
			xl.focusSheet(2);
			String firstname = xl.getCelldata(i, "FirstName");
			String lastname = xl.getCelldata(i, "LastName");
			String zip = xl.getCelldata(i, "ZipCode");
			yip.fillUpDetails(firstname, lastname, zip);
			yip.clickContinue();
			OverviewPage op = new OverviewPage(browser, r);
			op.clickFinish();
			if (browser.exists(CompletePage.thankXP())) {
				System.out.println("Scenario successfully executed");
				r.pass("Verification passed = Thankyou msg appeared");
			} else {
				System.out.println("Scenario failed");
				r.fail("Scenario failed to execute succesfully");
			}
			r.endTest();
			r.flush();
			browser.close();
			xl.focusSheet(0);
		}
	}
}
