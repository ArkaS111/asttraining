package application.scripts;

import application.pages.LoginPage;
import application.pages.Navbar;
import application.pages.ProductsPage;
import library.Browser;
import library.Excel;
import library.Reporter;

public class LogoutTest {

	public static void main(String[] args) {
		Reporter r = new Reporter(".\\src\\test\\resources\\LogoutTestReport.html");
		Excel xl = new Excel(".\\src\\test\\resources\\TestData.xlsx");
		Browser browser = new Browser();
		xl.focusSheet(0);
		r.startTest("Logout Test");
		browser.init();
		browser.maximize();
		String url = xl.getCelldata(1, "URL");
		browser.navigateTo(url);
		String username = xl.getCelldata(1, "UserName");
		String password = xl.getCelldata(1, "Password");
		LoginPage loginPage = new LoginPage(browser , r);
		loginPage.login(username, password);
		ProductsPage productsPage = new ProductsPage(browser , r);
		productsPage.clickHamburgerMenu();
		Navbar nb = new Navbar(browser , r);
		nb.clickLogout();
		if (browser.existsId(loginPage.loginButtonId())) {
			System.out.println("Scenario successfully executed");
			r.pass("Verification passed = Login button appeared again");
		}
		else {
			System.out.println("Scenario failed");
			r.fail("Scenario failed to execute succesfully");
		}
		r.endTest();
		r.flush();
		browser.close();

	}

}
