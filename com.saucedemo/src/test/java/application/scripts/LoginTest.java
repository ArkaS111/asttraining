package application.scripts;

import application.pages.LoginPage;
import application.pages.ProductsPage;
import library.Browser;
import library.Excel;
import library.Reporter;

public class LoginTest {
	public static void main(String[] args) {
	Reporter r = new Reporter(".\\src\\test\\resources\\LoginTestReport.html");
	Excel xl = new Excel(".\\src\\test\\resources\\TestData.xlsx");
	Browser browser = new Browser();
	xl.focusSheet(0);
	r.startTest("LoginTest");
	browser.init();
	browser.maximize();
	String url = xl.getCelldata(1, "URL");
	browser.navigateTo(url);
	String username = xl.getCelldata(1, "UserName");
	String password = xl.getCelldata(1, "Password");
	LoginPage loginPage = new LoginPage(browser , r);
	loginPage.login(username, password);
	if (browser.exists(ProductsPage.pageTitleXP())) {
		System.out.println("Scenario successfully executed");
		r.pass("Verification passed = Products page shown");
	}
	else {
		System.out.println("Scenario failed");
		r.fail("Scenario failed to execute succesfully");
	}
	r.endTest();
	r.flush();
	browser.close();
}
}
