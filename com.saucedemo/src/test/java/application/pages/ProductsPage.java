package application.pages;

import library.Base;
import library.Browser;
import library.Reporter;

public class ProductsPage extends Base {
	
	public ProductsPage(Browser browser , Reporter r) {
		super(browser , r);
	}
	
	static String addToCartXP = "//div[text()='##LABEL##']//following::div[2]/button";
	static String removeXP = "//div[text()='##LABEL##']//following::div[2]/button";
	
	public String getAddToCartXP(String prdTitle) {
		return addToCartXP.replace("##LABEL##", prdTitle);
		
	}
	public String getRemoveXP(String prdTitle) {
		return addToCartXP.replace("##LABEL##", prdTitle);
		
	}
	
	
	public static String sortXP() {
		return "//select[@class = 'product_sort_container']";
	}
	
	public String cartXP() {
		return "//a[@class='shopping_cart_link']";
	}
	
	public String hamburgerMenuId() {
		return "react-burger-menu-btn";
	}
	
	public static String pageTitleXP() {
		return "//span[@class='title']";
	}
	
	public void clickHamburgerMenu() {
		browser.click(hamburgerMenuId());
		r.info("Clicked on hamburger menu");
	}
	
	public void clickAddtoCart(String prdTitle) {
		String addToCart = getAddToCartXP(prdTitle);
		browser.clickByXP(addToCart);
		r.info("Added product: " + prdTitle);
	}
	
	public void clickCarticon() {
		browser.clickByXP(cartXP());
	}
	public void clickRemove(String prdTitle) {
		String remove = getRemoveXP(prdTitle);
		browser.clickByXP(remove);
		r.info("Removed product: " + prdTitle);
	}

}
