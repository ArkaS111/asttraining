package application.pages;

import library.Base;
import library.Browser;
import library.Reporter;

public class LoginPage extends Base {

	public LoginPage(Browser browser, Reporter r) {
		super(browser, r);
	}

	public static String getUsernameId() {
		return "user-name";
	}

	public static String getPasswordName() {
		return "password";
	}
	
	public String loginButtonId() {
		return "login-button";
	}

	public void setUsername(String usernameId, String username) {
		browser.setText(usernameId, username);
		r.info("set username as:" + username);
	}

	public void setPassword(String passwordName, String password) {
		browser.setText(passwordName, password);
		r.info("set password as:" + password);
	}

	public void clickLoginButton(String loginId) {
		browser.click(loginId);
		r.info("Logged in");
	}

	public void login(String username, String password) {
		setUsername(getUsernameId(), username);
		setPassword(getPasswordName(), password);
		clickLoginButton(loginButtonId());
	}
}
