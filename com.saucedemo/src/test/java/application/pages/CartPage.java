package application.pages;

import library.Base;
import library.Browser;
import library.Reporter;

public class CartPage extends Base {
	public CartPage(Browser b , Reporter r) {
		super(b,r);
	}
	
	public String checkoutXP() {
		return "//button[@name='checkout']";
	}
	
	public static String allItemxp() {
		return "//div[@class='cart_item']";
	}
	
	public void clickCheckout() {
		browser.clickByXP(checkoutXP());
		r.info("checked out from cart");
	}
	
	

}
