package application.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import library.Base;
import library.Browser;
import library.Reporter;

public class Navbar extends Base{
	public Navbar(Browser b ,Reporter r) {
		super(b,r);
	}
	public String logOutId() {
		return "logout_sidebar_link";
	}
	
	public void clickLogout() {
		WebDriverWait w = browser.wait(5);
		w.until(ExpectedConditions.elementToBeClickable(By.id(logOutId())));
		browser.click(logOutId());
		r.info("Clicked on logout ");
	}

}
