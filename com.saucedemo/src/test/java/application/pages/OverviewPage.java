package application.pages;

import library.Base;
import library.Browser;
import library.Reporter;

public class OverviewPage extends Base{
	public OverviewPage(Browser b, Reporter r) {
		super(b,r);
	}

	public String finishID() {
		return "finish";
	}
	public void clickFinish() {
		browser.click(finishID());
	}
}
