package application.pages;

import library.Base;
import library.Browser;
import library.Reporter;

public class YourInformationPage extends Base{
	public YourInformationPage(Browser b, Reporter r) {
		super(b , r);
	}
	public String firstNameXP() {
		return "//input[@id='first-name']";
	}
	public String lastNameXP() {
		return "//input[@id='first-name']//following::input[1]";
	}
	public String zipCodeXP() {
		return "//input[@id='first-name']//following::input[2]";
	}
	public String continueXP() {
		return "//input[@id='continue']";
	}
	public void setFirstName(String firstNamexp, String firstname) {
		browser.setTextByXP(firstNamexp, firstname);
		r.info("entered first name as: " + firstname );
	}
	public void setLastName(String lastNamexp, String lastname) {
		browser.setTextByXP(lastNamexp, lastname);
		r.info("entered last name as: " + lastname );
	}
	public void setZipCode(String zipCodexp, String zipcode) {
		browser.setTextByXP(zipCodexp, zipcode);
		r.info("entered zipcode as: " + zipcode );
	}
	public void fillUpDetails(String firstname , String lastname , String zipcode) {
		setFirstName(firstNameXP(), firstname);
		setLastName(lastNameXP(), lastname);
		setZipCode(zipCodeXP(), zipcode);
	}
	public void clickContinue() {
		browser.clickByXP(continueXP());
	}
}
