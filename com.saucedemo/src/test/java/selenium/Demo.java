//package selenium;
//
//import application.pages.CartPage;
//import application.pages.CompletePage;
//import application.pages.OverviewPage;
//import application.pages.ProductsPage;
//import application.pages.YourInformationPage;
//import library.Browser;
//import library.Excel;
//import library.Reporter;
//
//public class Demo {
//	public static void main(String[] args) throws InterruptedException {
//		Reporter r = new Reporter(".\\src\\test\\resources\\Output.html");
//		Excel xl =new Excel(".\\src\\test\\resources\\TestData.xlsx");
//		r.startTest("demoTest");
//		Browser browser = new Browser();
//		browser.init();
//		browser.maximize();
//		browser.navigateTo("https://www.saucedemo.com");
//		String username = xl.getParticularData(1, 0);
//		String password = xl.getParticularData(1, 1);
//		browser.setText("user-name", username);
//		r.info("set username as:" + username);
//		browser.setTextByName("password", password);
//		r.info("set password as:" + password);
//		browser.click("login-button");
//		r.info("Logged in");
//		String addToCartXP = ProductsPage.getAddToCartXP("Sauce Labs Backpack");
//		browser.clickByXP(addToCartXP);
//		String addToCartXPPrd2 = ProductsPage.getAddToCartXP("Sauce Labs Bike Light");
//		browser.clickByXP(addToCartXPPrd2);
//		r.info("Added Product to cart");
//		String removeXPprd2 = ProductsPage.getRemoveXP("Sauce Labs Bike Light");
//		browser.clickByXP(removeXPprd2);
//		r.info("Removed one product");
//		browser.selectOptionByDisplay(ProductsPage.sortXP(), "Price (low to high)");
//		String cartXP = ProductsPage.cartXP();
//		browser.clickByXP(cartXP);
//		String checkoutXP = CartPage.checkoutXP();
//		browser.clickByXP(checkoutXP);
//		r.info("checked out from cart");
//		String firstNameXP = YourInformationPage.firstNameXP();
//		String lastNameXP = YourInformationPage.lastNameXP();
//		String zipCodeXP = YourInformationPage.zipCodeXP();
//		browser.setTextByXP(firstNameXP, "Arka");
//		browser.setTextByXP(lastNameXP, "S");
//		browser.setTextByXP(zipCodeXP, "500001");
//		String continueXP = YourInformationPage.continueXP();
//		browser.clickByXP(continueXP);
//		r.info("Entered necessary details");
//		String finishId = OverviewPage.finishID();
//		browser.click(finishId);
//		if (browser.exists(CompletePage.thankXP())) {
//			System.out.println("Scenario successfully executed");
//			r.pass("Verification passed = Thankyou msg appeared");
//		}
//		else {
//			System.out.println("Scenario failed");
//			r.fail("Scenario failed to execute succesfully");
//		}
//		r.endTest();
//		r.flush();
//		browser.close();
//		
//		
//	}
//
//}
