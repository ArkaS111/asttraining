package library;

import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Excel {
	String xlFilepath ;
	XSSFWorkbook wb;
	XSSFSheet sheet;
	Map<String, Integer> map = new HashMap<String,Integer>();

	public Excel(String filepath) {
		try {
			this.xlFilepath = filepath;
			FileInputStream fis = new FileInputStream(new File(this.xlFilepath));
			System.out.println("File input stream created");
			wb = new XSSFWorkbook(fis);
			//sheet = wb.getSheetAt(0);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void focusSheet(int n) {
		this.sheet = wb.getSheetAt(n);
		colTitles(0);
	}
	public void focusSheet(String sheetname) {
		this.sheet = wb.getSheet(sheetname);
		colTitles(0);
	}
	public int getrowsNum() {
		int rowsNum = sheet.getLastRowNum();
		return rowsNum;
	}
	public void colTitles(int rowNum) {
		Row rows = sheet.getRow(rowNum);
		int maxColNum = rows.getLastCellNum();
		for(int i = 0; i < maxColNum; i++) {
			Cell cell = rows.getCell(i);
			map.put(cell.getStringCellValue(), cell.getColumnIndex());
		}
		
	}
//	public void readSheetData() {
//		Iterator<Row> rows = sheet.iterator();
//		while (rows.hasNext()) {
//			Row currow = rows.next();
//			Iterator<Cell> cells = currow.cellIterator();
//			while (cells.hasNext()) {
//				Cell currcell = cells.next();
//				CellType ctype = currcell.getCellType();
//				String value = "";
//				if (ctype == CellType.STRING) {
//					value = currcell.getStringCellValue();
//				}
//				if (ctype == CellType.NUMERIC) {
//					value = "" + currcell.getNumericCellValue();
//				}
//				System.out.println("Value of Cells:" + value);
//			}
//		}
//	}
	
	public String getCelldata(int rowNum , String colName) {
		//colTitles(0);
		Cell cell = sheet.getRow(rowNum).getCell(map.get(colName));
		CellType ctype = cell.getCellType();
		String value = "";
		if (ctype == CellType.STRING) {
			value = cell.getStringCellValue();
		}
		if (ctype == CellType.NUMERIC) {
			value = "" + cell.getNumericCellValue();
		}
		return value;
	}

	public String getParticularData(int row, int col) {
		Row rows = sheet.getRow(row);
		Cell cell = rows.getCell(col);
		CellType ctype = cell.getCellType();
		String value = "";
		if (ctype == CellType.STRING) {
			value = cell.getStringCellValue();
		}
		if (ctype == CellType.NUMERIC) {
			value = "" + cell.getNumericCellValue();
		}
		return value;

	}
}
