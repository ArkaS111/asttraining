package library;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Browser {
	WebDriver driver;
	
	public void init() {
		init("chrome");
	}
	public void init(String browserType) {
		if (browserType.equalsIgnoreCase("chrome")) {
			WebDriverManager.chromedriver().setup();
			driver = new ChromeDriver();
		}
		if (browserType.equalsIgnoreCase("firefox")) {
			WebDriverManager.firefoxdriver().setup();
			driver = new FirefoxDriver();
		}
		if (browserType.equalsIgnoreCase("edge")) {
			WebDriverManager.edgedriver().setup();
			driver = new EdgeDriver();
		}
	}
	public void maximize() {
		driver.manage().window().maximize();
	}
	public void navigateTo(String url) {
		driver.get(url);
	}
	public void setText(String id, String text) {
		driver.findElement(By.id(id)).sendKeys(text);
	}
	public void setTextByName(String name, String text) {
		driver.findElement(By.name(name)).sendKeys(text);
	}
	public void setTextByXP(String xp, String text) {
		driver.findElement(By.xpath(xp)).sendKeys(text);
	}
	public void click(String id) {
		driver.findElement(By.id(id)).click();
	}
	public void clickByXP(String xp) {
		driver.findElement(By.xpath(xp)).click();
	}
	public void selectOptionByDisplay(String xp, String value) {
		WebElement sel_ele = driver.findElement(By.xpath(xp));
		Select sel = new Select(sel_ele);
		sel.selectByVisibleText(value);
	}
	
	public WebDriverWait wait(int time) {
		WebDriverWait w = new WebDriverWait(driver, Duration.ofSeconds(time));
		return w;
	}
	public boolean exists(String xp) {
		boolean ret = false;
		try {
			WebElement ele = driver.findElement(By.xpath(xp));
			ele.isDisplayed();
			ret = true;
		} catch (Exception e) {
			
		}
		
		return ret;
	}
	public boolean existsId(String id) {
		boolean ret = false;
		try {
			WebElement ele = driver.findElement(By.id(id));
			ele.isDisplayed();
			ret = true;
		} catch (Exception e) {
			
		}
		
		return ret;
	}
	public boolean checkNumberofPrd(int n , String xp) {
		boolean ret = false;
		try {
			List<WebElement> ele = driver.findElements(By.xpath(xp));
			if (n == ele.size()) {
				ret = true;
			}
		} catch (Exception e) {
			
		}
		
		return ret;
	}
	public void close() {
		driver.close();
	}

}
